﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace AppAgency
{
    public partial class Profile : Form
    {
        string connection = ConnectionSql.getStringConnection();
        ClientsAndRieltors CurrentUser;
        SignIn sI;
        DataSet dataset;
        SqlDataAdapter adapter;
        string role;

        public Profile(ClientsAndRieltors user, string role)
        {
            InitializeComponent();
            CurrentUser = user;
            this.role = role;
            label1.Text = $"Здравствуйте, \n {CurrentUser.GetName()} {CurrentUser.GetSurname()}";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            tabControl1.SelectedTab = settings;
            FillRieltors(comboBox4);
            FillRieltors(comboBox6);
            FillDistricts();
            if(this.role == "Client")
            {
                button3.Visible = false;
            } else
            {
                button4.Visible = false;
                button5.Visible = false;
            }
            FillTableSells();
            FillTableBuyers();
            dataGridView1.EnableHeadersVisualStyles = true;

            dataGridView2.EnableHeadersVisualStyles = true;
        }
        
        private void FillTableSells()
        {
            using (SqlConnection connect = new SqlConnection(connection))
            {
              
                adapter = new SqlDataAdapter($"Select Город, Тип_недвижимости,Мин_цена, Макс_цена, Мин_площадь, Макс_площадь,Мин_комнат, Макс_комнат, " +
                   $"Мин_этаж, Макс_этаж, Район From Потребности join Клиенты on Потребности.Код_клиента = Клиенты.Код_клиента join Риэлторы on Потребности.Код_риэлтора = Риэлторы.Код_риэлтора where Завершено = False", connect); // where Потребности.Код_риэлтора = {CurrentUser.id_client}
                dataset = new DataSet();
                adapter.Fill(dataset);
                dataGridView2.DataSource = dataset.Tables[0];

            }
        }

        private void FillTableBuyers()
        {
            using (SqlConnection connect = new SqlConnection(connection))
            {

                adapter = new SqlDataAdapter($"Select Город, Площадь, Количество_комнат, Этаж, Типы_недвижимости.Наименование as Тип, Объекты_недвижимости.Район From " +
                   $"Объекты_недвижимости join Предложения on Объекты_недвижимости.Код_недвижимости = Предложения.Код_недвижимости join Клиенты on Предложения.Код_клиента = Клиенты.Код_клиента" +
                   $" join Риэлторы on Предложения.Код_риэлтора = Риэлторы.Код_риэлтора join Типы_недвижимости on Объекты_недвижимости.Код_типа_недвжимости = Типы_недвижимости.Код_типа_недвижимости" +
                   $" Where Предложения.Код_риэлтора = {CurrentUser.id_client} and Завершено = False", connect);
                dataset = new DataSet();
                adapter.Fill(dataset);
                dataGridView1.DataSource = dataset.Tables[0];

            }
        }


        private void FillRieltors(ComboBox combo)
        {
            using (SqlConnection connect = new SqlConnection(connection))
            {
                adapter = new SqlDataAdapter("Select Фамилия, Имя, Отчество From Риэлторы", connect);
                dataset = new DataSet();
                adapter.Fill(dataset);
                combo.Items.Clear();
                for(int i = 0;i < dataset.Tables[0].Rows.Count;i++)
                {
                    combo.Items.Add(dataset.Tables[0].Rows[i][0].ToString() + " "  + dataset.Tables[0].Rows[i][1].ToString() + " " + dataset.Tables[0].Rows[i][2].ToString());
                }
            }
        }

        private void FillDistricts()
        {
            using (SqlConnection connect = new SqlConnection(connection))
            {
                adapter = new SqlDataAdapter("Select Наименованеи_района From Районы", connect);
                dataset = new DataSet();
                adapter.Fill(dataset);
                comboBox5.Items.Clear();
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    comboBox5.Items.Add(dataset.Tables[0].Rows[i][0].ToString());
                }
            }
        }


        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            sI = new SignIn();
            this.Hide();
            sI.Show();
        }

        private void Profile_FormClosed(object sender, FormClosedEventArgs e)
        {
            sI = new SignIn();
            this.Hide();
            sI.Show();
        }
       
        private void button2_Click(object sender, EventArgs e)
        {
         
            tabControl1.SelectedTab = settings;

               
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DialogResult d = MessageBox.Show("Подтвердите удаление профиля!", "Сообщение", MessageBoxButtons.OKCancel, MessageBoxIcon.Information); 

            if(d == DialogResult.OK)
            {
                if (role == "Client")
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        adapter = new SqlDataAdapter($"Update Клиенты set Удалён = 1 Where Код_клиента = {CurrentUser.id_client}", connect);
                        dataset = new DataSet();
                        adapter.Fill(dataset);

                    }
                }
                else
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        adapter = new SqlDataAdapter($"Update Риэлторы set Удалён = 1 Where Код_риэлтора = {CurrentUser.id_client}", connect);
                        dataset = new DataSet();
                        adapter.Fill(dataset);
                    }
                }
                MessageBox.Show("Вы удалили свой профиль! Сейчас произойдёт переход на форму авторизации.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                sI = new SignIn();
                this.Hide();
                sI.Show();
            } else 
            {

            }
           
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = settings;
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void buy_Click(object sender, EventArgs e)
        {

        }

        private void deal_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            if(role == "Client")
            {
                if (textBox2.Text != "" || comboBox1.Text != "")
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        adapter = new SqlDataAdapter($"Update Клиенты set Почта = '{textBox2.Text}{comboBox1.Text}' Where Код_клиента = {CurrentUser.id_client}", connect);
                        dataset = new DataSet();
                        adapter.Fill(dataset);
                    }
                }

                if (textBox14.Text != "" || comboBox2.Text != "")
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        adapter = new SqlDataAdapter($"Update Клиенты set Телефон = '{comboBox2.Text}{textBox14.Text}' Where Код_клиента = {CurrentUser.id_client}", connect);
                        dataset = new DataSet();
                        adapter.Fill(dataset);
                    }
                }
                if (textBox1.Text != "")
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        adapter = new SqlDataAdapter($"Update Клиенты set Пароль = '{textBox1.Text}' Where Код_клиента = {CurrentUser.id_client}", connect);
                        dataset = new DataSet();
                        adapter.Fill(dataset);
                    }
                }
                if (textBox3.Text != "")
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        adapter = new SqlDataAdapter($"Update Клиенты set Фамилия = '{textBox3.Text}' Where Код_клиента = {CurrentUser.id_client}", connect);
                        dataset = new DataSet();
                        adapter.Fill(dataset);
                    }
                }
                if (textBox4.Text != "")
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        adapter = new SqlDataAdapter($"Update Клиенты set Имя = '{textBox4.Text}' Where Код_клиента = {CurrentUser.id_client}", connect);
                        dataset = new DataSet();
                        adapter.Fill(dataset);
                    }
                }
                if (textBox5.Text != "")
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        adapter = new SqlDataAdapter($"Update Клиенты set Отчество = '{textBox5.Text}' Where Код_клиента = {CurrentUser.id_client}", connect);
                        dataset = new DataSet();
                        adapter.Fill(dataset);
                    }
                }
                MessageBox.Show("Данные профиля изменены", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } else
            {
                if (textBox2.Text != "" || comboBox1.Text != "")
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        adapter = new SqlDataAdapter($"Update Риэлторы set Почта = '{textBox2.Text}{comboBox1.Text}' Where Код_риэлтора = {CurrentUser.id_client}", connect);
                        dataset = new DataSet();
                        adapter.Fill(dataset);
                    }
                }

                if (textBox14.Text != "" || comboBox2.Text != "")
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        adapter = new SqlDataAdapter($"Update Риэлторы set Телефон = '{comboBox2.Text}{textBox14.Text}' Where Код_риэлтора = {CurrentUser.id_client}", connect);
                        dataset = new DataSet();
                        adapter.Fill(dataset);
                    }
                }
                if (textBox1.Text != "")
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        adapter = new SqlDataAdapter($"Update Риэлторы set Пароль = '{textBox1.Text}' Where Код_риэлтора = {CurrentUser.id_client}", connect);
                        dataset = new DataSet();
                        adapter.Fill(dataset);
                    }
                }
                if (textBox3.Text != "")
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        adapter = new SqlDataAdapter($"Update Риэлторы set Фамилия = '{textBox3.Text}' Where Код_риэлтора = {CurrentUser.id_client}", connect);
                        dataset = new DataSet();
                        adapter.Fill(dataset);
                    }
                }
                if (textBox4.Text != "")
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        adapter = new SqlDataAdapter($"Update Риэлторы set Имя = '{textBox4.Text}' Where Код_риэлтора = {CurrentUser.id_client}", connect);
                        dataset = new DataSet();
                        adapter.Fill(dataset);
                    }
                }
                if (textBox5.Text != "")
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        adapter = new SqlDataAdapter($"Update Риэлторы set Отчество = '{textBox5.Text}' Where Код_риэлтора = {CurrentUser.id_client}", connect);
                        dataset = new DataSet();
                        adapter.Fill(dataset);
                    }
                }
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                textBox14.Text = "";
                MessageBox.Show("Данные профиля изменены", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           

          
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkBox1.Checked) textBox1.UseSystemPasswordChar = false;
            else textBox1.UseSystemPasswordChar = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = deal;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = buy;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = sell;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if(comboBox4.Text == "" || textBox15.Text == "")
            {
                MessageBox.Show("Заполните все поля!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } else
            {
                string fioRiel = comboBox4.Text;
                string id_riel, id_home;

                using (SqlConnection connect = new SqlConnection(connection))
                {
                    adapter = new SqlDataAdapter($"Select Код_риэлтора From Риэлторы Where Фамилия = '{fioRiel.Split(' ')[0]}' and Имя = '{fioRiel.Split(' ')[1]}' and Отчество = '{fioRiel.Split(' ')[2]}'", connect);
                    dataset = new DataSet();
                    adapter.Fill(dataset);
                    id_riel = dataset.Tables[0].Rows[0][0].ToString();

                    adapter = new SqlDataAdapter($"Select Код_недвижимости From Объекты_недвижимости Where Кадастровый_номер = '{maskedTextBox2.Text}'", connect);
                    dataset = new DataSet();
                    adapter.Fill(dataset);
                   
                    if (dataset.Tables[0].Rows.Count > 0)
                    {
                        id_home = dataset.Tables[0].Rows[0][0].ToString();
                        SqlCommand comm = new SqlCommand("insert into Предложения values(@id_home,@id_client,@id_riel,@cost,@stat)", connect);
                        comm.Parameters.AddWithValue("@id_home", id_home);
                        comm.Parameters.AddWithValue("@id_client", CurrentUser.id_client);
                        comm.Parameters.AddWithValue("@id_riel", id_riel);
                        comm.Parameters.AddWithValue("@cost", textBox15.Text);
                        comm.Parameters.AddWithValue("@stat", "False");
                        connect.Open();
                        try
                        {
                            comm.ExecuteNonQuery();
                        }
                        catch(Exception exp)
                        {
                            MessageBox.Show(exp.ToString(), "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        MessageBox.Show("Заявка на продажу отправлена!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Недвижимость с таким кадастровым номером не найдена!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }


                }
            }
            
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (comboBox6.Text != "")
            {


                string fioRiel = comboBox6.Text;
                string id_riel;
                using (SqlConnection connect = new SqlConnection(connection))
                {
                    adapter = new SqlDataAdapter($"Select Код_риэлтора From Риэлторы Where Фамилия = '{fioRiel.Split(' ')[0]}' and Имя = '{fioRiel.Split(' ')[1]}' and Отчество = '{fioRiel.Split(' ')[2]}'", connect);
                    dataset = new DataSet();
                    adapter.Fill(dataset);
                    id_riel = dataset.Tables[0].Rows[0][0].ToString();

                    SqlCommand comm = new SqlCommand("insert into Потребности(Город,Тип_недвижимости,Мин_цена, Макс_цена, Код_риэлтора, Код_клиента,Мин_площадь, Макс_площадь, Мин_комнат, Макс_комнат, Мин_этаж, Макс_этаж, Район, Завершено)" +
                        "values(@city,@type,@min_cost,@max_cost,@id_riel,@id_client,@min_area,@max_area,@min_comnat,@max_comnat,@min_floor,@max_floor,@district,@stat)", connect);
                    comm.Parameters.AddWithValue("@city", comboBox3.Text);
                    comm.Parameters.AddWithValue("@type", comboBox7.Text);
                    comm.Parameters.AddWithValue("@min_cost", textBox6.Text);
                    comm.Parameters.AddWithValue("@max_cost", textBox7.Text);
                    comm.Parameters.AddWithValue("@id_riel", id_riel);
                    comm.Parameters.AddWithValue("@id_client", CurrentUser.id_client);
                    comm.Parameters.AddWithValue("@min_area", textBox9.Text);
                    comm.Parameters.AddWithValue("@max_area", textBox8.Text);
                    comm.Parameters.AddWithValue("@min_comnat", textBox11.Text);
                    comm.Parameters.AddWithValue("@max_comnat", textBox10.Text);
                    comm.Parameters.AddWithValue("@min_floor", textBox13.Text);
                    comm.Parameters.AddWithValue("@max_floor", textBox12.Text);
                    comm.Parameters.AddWithValue("@district", comboBox5.Text);
                    comm.Parameters.AddWithValue("@stat", "False");
                    connect.Open();
                    try
                    {
                        comm.ExecuteNonQuery();
                    }
                    catch(Exception exp)
                    {
                        MessageBox.Show(exp.ToString(), "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
                MessageBox.Show("Заявка на покупку отправлена!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBox6.Text = "";
                textBox7.Text = "";
                textBox8.Text = "";
                textBox9.Text = "";
                textBox10.Text = "";
                textBox11.Text = "";
                textBox12.Text = "";
                textBox13.Text = "";

            } else
            {
                MessageBox.Show("Выберите риэлтора!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = main;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            DialogResult d = MessageBox.Show("Подтвердите завершение сделки", "Сообщение", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if(d == DialogResult.OK)
            {
                int id_buy, id_sell;
                id_buy = dataGridView1.CurrentCell.RowIndex + 1;
                id_sell = dataGridView2.CurrentCell.RowIndex + 1;
                if (dataGridView1.CurrentCell != null || dataGridView2.CurrentCell != null)
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        SqlCommand comm = new SqlCommand("insert into Сделка values(@id_buy,@id_sell,@date)", connect);
                        comm.Parameters.AddWithValue("@id_buy", id_buy);
                        comm.Parameters.AddWithValue("@id_sell", id_sell);
                        comm.Parameters.AddWithValue("@date", DateTime.Today);
                        connect.Open();
                        try
                        {
                            comm.ExecuteNonQuery();
                        }
                        catch (Exception exp)
                        {
                            MessageBox.Show(exp.ToString(), "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                    }
                    MessageBox.Show("Сделка совершена!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FillTableSells();
                    FillTableBuyers();
                }
                else
                {
                    MessageBox.Show("Выберите потребность или предложение", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

           
           

        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)Keys.Back)
            {
                return;
            }
            if (Char.IsDigit(e.KeyChar))
            {
                return;
            }
            e.Handled = true;
        }


        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)Keys.Back)
            {
                return;
            }
            if (Char.IsDigit(e.KeyChar))
            {
                return;
            }
            e.Handled = true;
        }

        private void textBox9_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)Keys.Back)
            {
                return;
            }
            if (Char.IsDigit(e.KeyChar))
            {
                return;
            }
            e.Handled = true;
        }

        private void textBox8_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)Keys.Back)
            {
                return;
            }
            if (Char.IsDigit(e.KeyChar))
            {
                return;
            }
            e.Handled = true;
        }

        private void textBox11_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)Keys.Back)
            {
                return;
            }
            if (Char.IsDigit(e.KeyChar))
            {
                return;
            }
            e.Handled = true;
        }

        private void textBox10_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)Keys.Back)
            {
                return;
            }
            if (Char.IsDigit(e.KeyChar))
            {
                return;
            }
            e.Handled = true;
        }

        private void textBox13_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)Keys.Back)
            {
                return;
            }
            if (Char.IsDigit(e.KeyChar))
            {
                return;
            }
            e.Handled = true;
        }

        private void textBox12_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)Keys.Back)
            {
                return;
            }
            if (Char.IsDigit(e.KeyChar))
            {
                return;
            }
            e.Handled = true;
        }

        private void Profile_Load(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = main;
        }

        private void textBox15_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox15_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)Keys.Back)
            {
                return;
            }
            if (Char.IsDigit(e.KeyChar))
            {
                return;
            }
            e.Handled = true;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";
            textBox10.Text = "";
            textBox11.Text = "";
            textBox12.Text = "";
            textBox13.Text = "";
            comboBox7.SelectedIndex = -1;
            comboBox6.SelectedIndex = -1;
            comboBox5.SelectedIndex = -1;
        }
    }
}
