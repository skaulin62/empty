﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AppAgency
{
    public partial class SignUp : Form
    {
        string connection = ConnectionSql.getStringConnection();

        public SignUp()
        {
            InitializeComponent();
        }

        private bool CheckedPassword(string password)
        {
           if(!password.Any(Char.IsDigit))
            {
                return false;
            }
           if(!password.Any(Char.IsUpper))
            {
                return false;
            }
           if(password.Length < 6)
            {
                return false;
            }
           

            return true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == ""  || textBox6.Text == "" || comboBox1.Text == "" || comboBox2.Text == "")
            {
                MessageBox.Show("Заполните все поля!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } else
            {

                if(CheckedPassword(textBox1.Text) == true)
                {
                    using (SqlConnection connect = new SqlConnection(connection))
                    {
                        SqlCommand comm = new SqlCommand("insert into Клиенты values(@surname, @name, @lastname, @phone, @mail,@password, @IsDeleted)", connect);
                        comm.Parameters.AddWithValue("@surname", textBox3.Text);
                        comm.Parameters.AddWithValue("@name", textBox4.Text);
                        comm.Parameters.AddWithValue("@lastname", textBox5.Text);
                        comm.Parameters.AddWithValue("@phone", comboBox2.Text + textBox6.Text);
                        comm.Parameters.AddWithValue("@mail", textBox2.Text + comboBox1.Text);
                        comm.Parameters.AddWithValue("@password", textBox1.Text);
                        comm.Parameters.AddWithValue("@IsDeleted", "False");
                        connect.Open();
                        try
                        {
                            comm.ExecuteNonQuery();

                        }
                        catch
                        {
                            MessageBox.Show("Что-то пошло не так!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    MessageBox.Show("Вы успешно зарегистрировались!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Hide();
                    SignIn sI = new SignIn();
                    sI.Show();
                } else
                {
                    MessageBox.Show("Слишком простой пароль!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
               
            }

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkBox1.Checked) textBox1.UseSystemPasswordChar = true;
            else textBox1.UseSystemPasswordChar = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            SignIn sI = new SignIn();
            sI.Show();
        }

        private void SignUp_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            SignIn sI = new SignIn();
            sI.Show();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)Keys.Back)
            {
                return;
            }
            if (Char.IsDigit(e.KeyChar))
            {
                return;
            }
            e.Handled = true;
        }
    }
}
