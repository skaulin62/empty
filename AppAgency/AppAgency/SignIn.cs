﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient; 

namespace AppAgency
{
    public partial class SignIn : Form
    {
        string connection = ConnectionSql.getStringConnection();
        DataSet dataset;
        SqlDataAdapter adapter;
        Profile prof;
        ClientsAndRieltors clientsAndRieltors;
        public SignIn()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool Open = false;
       

            using(SqlConnection connect =new SqlConnection(connection))
            {
                adapter = new SqlDataAdapter("Select * From Клиенты Where Удалён = 0", connect);
                dataset = new DataSet();
                adapter.Fill(dataset);
                
                for(int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    if(textBox2.Text == dataset.Tables[0].Rows[i][4].ToString() || textBox2.Text == dataset.Tables[0].Rows[i][5].ToString())
                    {
                        if(textBox1.Text == dataset.Tables[0].Rows[0][6].ToString())
                        {
                            Open = true;
                            clientsAndRieltors = new ClientsAndRieltors(dataset.Tables[0].Rows[i][0].ToString(), dataset.Tables[0].Rows[i][1].ToString(), dataset.Tables[0].Rows[i][2].ToString(),
                                dataset.Tables[0].Rows[i][3].ToString(), dataset.Tables[0].Rows[i][4].ToString(), dataset.Tables[0].Rows[i][5].ToString(), dataset.Tables[0].Rows[i][6].ToString(), dataset.Tables[0].Rows[i][7].ToString(), "0");
                            prof = new Profile(clientsAndRieltors, "Client");
                            this.Hide();
                            prof.Show();
                            break;
                        }
                    }
                }

                adapter = new SqlDataAdapter("Select * From Риэлторы Where Удалён = 0", connect);
                dataset = new DataSet();
                adapter.Fill(dataset);
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    if (textBox2.Text == dataset.Tables[0].Rows[i][5].ToString() || textBox2.Text == dataset.Tables[0].Rows[i][6].ToString())
                    {
                        if (textBox1.Text == dataset.Tables[0].Rows[i][7].ToString())
                        {
                            Open = true;
                            
                            clientsAndRieltors = new ClientsAndRieltors(dataset.Tables[0].Rows[i][0].ToString(), dataset.Tables[0].Rows[i][1].ToString(), dataset.Tables[0].Rows[i][2].ToString(),
                                dataset.Tables[0].Rows[i][3].ToString(), dataset.Tables[0].Rows[i][5].ToString(), dataset.Tables[0].Rows[i][6].ToString(), dataset.Tables[0].Rows[i][7].ToString(), dataset.Tables[0].Rows[i][8].ToString(), dataset.Tables[0].Rows[i][4].ToString());
                            prof = new Profile(clientsAndRieltors, "Rieltor");
                            this.Hide();
                            prof.Show();
                            break;
                        }
                    }
                }
                if(Open == false)
                {
                    MessageBox.Show("Не удалось войти!", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            SignUp sU = new SignUp();
            sU.Show();
        }

        private void SignIn_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
